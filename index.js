const { ApolloServer } = require('apollo-server')
const mongoose = require('mongoose')

//imports
const { MONGO_URI } = require('./config.js')
const post = require('./models/post')
const user = require('./models/user')
const typeDefs = require('./graphql/typeDefs')
const resolvers = require('./graphql/resolvers')


const server = new ApolloServer({
        typeDefs, resolvers
    })

  
//Mongoose connection
mongoose
.connect(MONGO_URI, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true })
    .then(() => {
        console.log('MongoDB Connected!!')
        return server.listen({port: 5000})
    }).then((res) => {
        console.log(`Server running at ${res.url}`)
    }).catch(err => {
        console.log(`Connection error: ${err.message}`)
    })

