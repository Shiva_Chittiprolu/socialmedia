const postsResolvers = require('./posts')
const userResolver = require('./users')

module.exports = {
    Query: {
        ...postsResolvers.Query
    },
    Mutation: {
        ...userResolver.Mutation
    }
}
