const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')

const User = require('../../models/user')
const SECRETKEY = require('../../config')

module.export = {
    Mutation: {
        async register(_,{username, email, password, confirmedPassword }, context, info){
            //ToDO : Validate data
            //valiadte user doesn't alreadt exist
            //todo: hash password
            password = await bcrypt.hash(password, 12)

            const newUser = new User( {
                email,
                username,
                password,
                createdAt: new Date().toISOString()
            })

           const res = await newUser.save()
            const token = jwt.sign({
                id: res.id,
                email: res.email,
                password : res.password
            }, ) 
        

        }
    }
}