const { gql } = require('apollo-server')

module.exports = gql`
    type Query{
        getPosts: [Post]
    }

    type Post{
        id: ID!
        body: String!
        createdAt: String!
        username: String
    }

    type User{
        id: ID!
        email: String!
        token: String!
        username: String!
        createdAt: String!
         
    }

    input RegisterInput{
        username: String!
        password: String!
        confirmedPassword: String!
        email: String!
    }
    type Mutation{
        register(registerInput: RegisterInput): User!
    } 
`
